import pathlib
import json

import telegram_bot
from util import totp
from util import auth

from flask import Flask, request, jsonify, Response
app = Flask(__name__)

BOT = None

users_waiting = {}

@app.route("/check", methods=["POST"])
def check():
    user_hash = request.form["hash"]
    
    # Get the user from the database
    users_file = pathlib.Path("data/users.json")    
    if not users_file.exists():
        return jsonify(message="Internal error (users file not found)"), 500

    with users_file.open("r") as f:
        users = json.load(f)
    
    user = users.get(user_hash, None)
    if user is None:    
        return jsonify(message="You are not registered yet"), 401

    # Prepares the user lock
    auth.add_user_to_waiting_list(user_hash, user)    

    # Sends the telegram message to the user
    telegram_bot.send_auth_keyboard(BOT, user["telegram_id"], user_hash)

    # Wait until user allows to continue
    allowed = auth.wait_for_confirmation(user_hash)
    auth.remove_user_from_waiting_list(user_hash)
    
    if not allowed:
        return jsonify(message="Timeout ocurred"), 408
    
    mytotp = totp.create_totp(user["token"], totp.code_timeout)
    return jsonify(code=mytotp.now()), 200

@app.errorhandler(404)
def not_found(error):
    return jsonify({"message": "Error, could not found!"}), 404

def run():
    global BOT
    # Ensures that the bot Object is created    
    telegram_bot.wait_for_updater()
    BOT = telegram_bot.updater.bot
    print("OK")

    app.run(host='0.0.0.0', port=8080)
