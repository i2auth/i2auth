import os
import hashlib
import bcrypt
import pyotp
import base64

register_timeout = 120
code_timeout = 30

def create_salt(size):    
    return bcrypt.gensalt()

def create_hash(token, salt):    
    return bcrypt.hashpw(token.encode('utf-8'), salt)

def create_totp(token, timeout):
    return pyotp.TOTP(token, interval=timeout)
