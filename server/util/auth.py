import threading

auth_timeout = 10

users_waiting = {}

def add_user_to_waiting_list(user_hash, user):
    """ Adds the user to a waiting list. If the user is in the list it means
        that he is going to ask for authentication """

    users_waiting[user_hash] = {
        "cv": threading.Condition(),
        "user": user,
        "allowed": False
    }

def wait_for_confirmation(user_hash): 
    """ Sets the user waiting on a lock until he confirm the authentication
        or the timeout occur.
        
        Returns True if Condition is notified before timeout, False otherwise """       
        
    cv = users_waiting[user_hash]["cv"]
    with cv:    
        not_timeout = cv.wait(timeout=auth_timeout) # returns Flase if timeout happens    

    return not_timeout

def confirm_user_authentication(user_hash):
    """ Notify the user. It should be executed when the user confirms his 
        identity """

    cv = users_waiting[user_hash]["cv"]
    with cv:
        cv.notifyAll()
        users_waiting[user_hash]["allowed"] = True

def remove_user_from_waiting_list(user_hash):
    """ Removes the user from the waiting list, the authentication protocol 
        has finished """

    del users_waiting[user_hash]