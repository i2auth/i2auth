# i2Auth Server

i2Auth web server & telegram bot that makes possible the use of telegram to authenticate a user

## Contents table

- [i2Auth Server](#i2auth-server)
  - [Contents table](#contents-table)
  - [Getting started](#getting-started)
    - [Requirements](#requirements)
    - [Instalation](#instalation)
  - [Bot commands](#bot-commands)
    - [register](#register)
    - [get_code](#getcode)
  - [Webserver endpoints](#webserver-endpoints)
    - [request_auth](#requestauth)
    - [check](#check)
    - [confirm_auth](#confirmauth)
  - [License](#license)

## Getting started

If you want to set up your own server follow the [**Requirments**](#requirements) and [**Instalation**](#instalation) section


**[Go up](#contents-table)**

### Requirements

The project uses [Pipenv][1] to isolate all installed packages from the host system.

To install pipenv execute the following command

```sh
pip install pipenv
```

Pipenv defines as all the project dependencies in the [Pipfile](Pipfile) file. To install them you can use `pipenv install`.

The dependencies are the following:

- [Python Telegram Bot][2] for the Telegram bot
- [Flask][3] as the web server
- [PyYAML][4] allows to parse yaml files
- [pyotp][5] allows to create Time based one time password

**[Go up](#contents-table)**

### Instalation

First of all you will to create a Telegram Bot using [BotFather][6].

Before creating your bot you must set the bot token in the server config file `etc/telegram_bot_config.yaml`.

```yaml
---
token: <TOKEN>
```

To run the server enter into the pipenv and execute the `main.py` file

```sh
# Enter into the pipenv
pipenv shell

# Run the server
python3 main.py
```

Or launch if from outsite of the **venv** with `pipenv run python main.py`

**[Go up](#contents-table)**

## Bot commands

### register

- Data: token
- Example: `/register 123123123123`

Register your user to the server. The token will be the
secret that both client and server will use to create the TOTP code. User will
be stored in a key-value database identified by a hash of his token.

### get_code

- Example: `/get_code`

If you are already registered, returns a TOTP code.

**[Go up](#contents-table)**

## Webserver endpoints

### request_auth

- Method: Post
- Data: hash
- Example: `localhost:8080/request_auth`

Register the user identified by the hash. This puts the user in a list of users 
that are waiting for authorization.

### check

- Method: Post
- Data: hash
- Return: {message=code}
- Example: `localhost:8080/check`

Check if the user is already authorized to continue. Returns a totp code that the
client app will need to compare with its own totp code. If user is not
authorized, it will return -1.

### confirm_auth

- Method: Post
- Data: hash
- Return: {message=code}
- Example: `localhost:8080/confirm_auth`

Remove the user from the list of users that are waiting for authorization.

**[Go up](#contents-table)**

## License

This project is licensed under the GPLv3 License. See the [LICENSE](LICENSE) file for the full license text.

**[Go up](#contents-table)**

[1]: https://docs.pipenv.org/
[2]: https://github.com/python-telegram-bot/python-telegram-bot
[3]: http://flask.pocoo.org/
[4]: http://pyyaml.org/wiki/PyYAMLDocumentation
[5]: https://github.com/pyauth/pyotp
[6]: https://telegram.me/BotFather