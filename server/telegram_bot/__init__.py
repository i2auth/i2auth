import os
import yaml

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import CallbackQueryHandler

from threading import Condition

updater_cv = Condition()
updater = None
config_path = os.path.join(os.environ['BASE_PATH'], 'etc', 'telegram_bot_config.yaml')

# Defines the inline keyboard id and text
inline_keyboard_buttons = {
    "allow": {
        "id": "1",
        "text": "Allow"
    },
    "refresh": {
        "id": "2",
        "text": "Refresh code"
    }
}

from .handler import command
from .handler import message
from .handler import inline_query
from .sender import send_auth_keyboard

# Load config
with open(config_path, 'r') as conf_file:
    config = yaml.load(conf_file)

def is_updater_ready():
    return updater is not None

def wait_for_updater():
    """ Wait until updater object is defined """
    while not is_updater_ready():
        with updater_cv:    
            updater_cv.wait()

def notify_updater_is_ready():
    """ Notify all the threads that the updater object is not None anymore """
    with updater_cv:
        updater_cv.notify_all()   

def run():
    global updater
    updater = Updater(token=config['token'])
    notify_updater_is_ready()
    dispatcher = updater.dispatcher

# ------------------------------------------------------------------------------
    # Command handlers
    start_handler = CommandHandler('start', command.start)
    help_handler = CommandHandler('help', command.help)
    about_handler = CommandHandler('about', command.about)

    register_handler = CommandHandler('register', command.register, pass_args=True)
    get_code_handler = CommandHandler('get_code', command.get_code, pass_args=True)
    
    # Message handlers
    plain_text_handler = MessageHandler(Filters.text, message.plain_text)

    # Inline queries handlers
    inline_queries_handler = CallbackQueryHandler(inline_query.auth_keyboard)


# ------------------------------------------------------------------------------

    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(about_handler)

    dispatcher.add_handler(register_handler)
    dispatcher.add_handler(get_code_handler)

    dispatcher.add_handler(plain_text_handler)

    dispatcher.add_handler(inline_queries_handler)

    # Starting the bot
    updater.start_polling()
