import json
from telegram_bot import inline_keyboard_buttons as keyboard_data
from util import totp
from util import auth
from telegram import ChatAction, InlineKeyboardButton, InlineKeyboardMarkup

def send_auth_keyboard(bot, user_id, user_hash):
    """ Creates the keyboard and the message and sends it to he user """

    allow_button_data = "{}#{}".format(keyboard_data["allow"]["id"],user_hash)    
    refresh_button_data = "{}#{}".format(keyboard_data["refresh"]["id"],user_hash)
        
    keyboard = [
        [InlineKeyboardButton(keyboard_data["allow"]["text"], callback_data=allow_button_data)],
        [InlineKeyboardButton(keyboard_data["refresh"]["text"], callback_data=refresh_button_data)]
    ]
    print(allow_button_data)
    print(refresh_button_data)    

    user = auth.users_waiting[user_hash]["user"]
    mytotp = totp.create_totp(user["token"], totp.code_timeout)

    status = "Not allowed \U0001F512"
    code = mytotp.now()

    message = (
        "Choose one action:\n"
        "\n"
        "Current status: *{}*\n"
        "Code: *{}*\n"
    ).format(status, code)
        
    
    bot.send_message(chat_id=user_id, parse_mode='markdown', text=message, reply_markup=InlineKeyboardMarkup(keyboard))