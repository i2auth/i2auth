from telegram_bot import inline_keyboard_buttons as keyboard_data
from util import totp
from util import auth
from telegram import ChatAction, InlineKeyboardButton, InlineKeyboardMarkup

def auth_keyboard(bot, update):    
    """ Function that it's executed when one keyboard button is pressed
        It will recreate the message according to the button pressed. It will
        generate a new TOTP code and if the button pressed is the one that allows
        the authentication, it will notify the web server """
        
    query = update.callback_query    
    chat_id = update.effective_message.chat_id
    message_id = query.message.message_id

    # Data format is "Action#Hash"
    data = query.data.split("#")
    pressed_button = data[0]

    # If user is not asking for authentication or expired
    user_hash = data[1]
    if user_hash not in auth.users_waiting:
        # bot.delete_message(chat_id=chat_id, message_id=message_id)
        message = "Message expired"
        bot.edit_message_text(
        chat_id=chat_id, parse_mode='markdown', text=message, message_id=message_id)
        return 
    
    # Gets the user
    user = auth.users_waiting[user_hash]["user"]
    mytotp = totp.create_totp(user["token"], totp.code_timeout)
    
    # If users pressed the 'allow' button
    if pressed_button == keyboard_data["allow"]["id"]:        
        auth.confirm_user_authentication(user_hash)        
        
    # Recreate the keyboard and the message
    allow_button_data = "{}#{}".format(keyboard_data["allow"]["id"],user_hash)    
    refresh_button_data = "{}#{}".format(keyboard_data["refresh"]["id"],user_hash)
        
    keyboard = [
        [InlineKeyboardButton(keyboard_data["allow"]["text"], callback_data=allow_button_data)],
        [InlineKeyboardButton(keyboard_data["refresh"]["text"], callback_data=refresh_button_data)]
    ]

    status = ("Allowed \U0001F513" if auth.users_waiting[user_hash]["allowed"] 
        else "Not allowed \U0001F512")
    code = mytotp.now()

    message = (
        "Choose one action:\n"
        "\n"
        "Current status: *{}*\n"
        "Code: *{}*\n"
    ).format(status, code)

    bot.edit_message_text(
        chat_id=chat_id, parse_mode='markdown', text=message, 
        message_id=message_id, reply_markup=InlineKeyboardMarkup(keyboard))

