import os 
import time
import json

from telegram_bot import logging
from util import totp

logger = logging.getLogger(__name__)

# ------------------------------------------------------------------------------

def start(bot, update):
    """ Function that it's executed when the command '/start' is received """
        
    user_id = update.effective_message.chat_id

    message = """ Hello """    
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')

def help(bot, update):
    """ Function that it's executed when the command '/help' is received """
    
    user_id = update.effective_message.chat_id

    message = """ help """
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')

def about(bot, update):
    """ Function that it's executed when the command '/about' is received """
        
    user_id = update.effective_message.chat_id
    
    message = """ about """    
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')


# ------------------------------------------------------------------------------
def register(bot, update, args):
    """ Function that it's executed when the command '/register <TOKEN>' is received """
    user_id = update.effective_message.chat_id

    if(len(args) != 1):
        message = """ Just one argument needed (token) """
        bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
        return    
    
    # Creates the totp and stores the user token    
    token = args[0]
    salt = totp.create_salt(64)
    mytotp = totp.create_totp(token, totp.register_timeout)
    hashed_token = totp.create_hash(token, salt)    

    # Store the user in a database (file)
    with open("data/users.json", "r+") as f:
        # Loads the users file
        users = json.load(f)
        
        # Delete the file
        f.seek(0)        
        f.truncate(0)    

        # Delete the user if the telegram id already exists
        for hash_token, user in users.items():
            if(user["telegram_id"] == user_id):
                del users[hash_token]
                break  

        # Register the current user
        users[hashed_token.decode("utf-8")] = {            
            "telegram_id":user_id,
            "salt": salt.decode("utf-8"),
            "token": token        
        }
        
        # Saves the new user list
        json.dump(users, f)   
        
    # Send the message with the code
    message = (
        "Welcome to i2auth registration!\n\n"
        "You will find the code and the hash that you have to insert into the"
        "client app below!\n\n"
        "*Code:* {}\n"
        "*Hash:* {}"
    ).format(        
        mytotp.now(),
        hashed_token.decode("utf-8")
    )

    bot.send_message(chat_id=user_id, text=message, parse_mode="markdown")

def get_code(bot, update, args):
    """ Function that it's executed when the command '/get_token' is received """
    user_id = update.effective_message.chat_id    

    # Search for the user in the database
    with open("data/users.json", "r") as f:
        # Loads the users file
        users = json.load(f)
    
    target_user = None    
    for hash_token, user in users.items():
        if(user["telegram_id"] == user_id):    
            target_user = user
            break
    
    if(target_user is None):
        message = "You are not registered yet!"
        bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')
    
    # Generate the token
    mytotp = totp.create_totp(target_user["token"], totp.code_timeout)
    message = "Your code is: {}".format(mytotp.now())
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')