def plain_text(bot, update):
    """ Function than it's executed when a plain text is received. """
    
    user_id = update.effective_message.chat_id
    text = update.message.text

    message = "Received: {}".format(text)    
    bot.send_message(chat_id=user_id, text=message, parse_mode='markdown')

