import os
import threading
import json
os.environ['BASE_PATH'] = os.path.abspath(os.path.dirname(__file__))

import telegram_bot
import web_server
#from util import totp

if __name__ == "__main__":
    telegram_bot_thread = threading.Thread(target=telegram_bot.run)
    web_server_thread = threading.Thread(target=web_server.run)    

    telegram_bot_thread.start()
    web_server_thread.start()

    telegram_bot_thread.join()
    web_server_thread.join()    
