package token

import (
	"crypto/sha512"
	"encoding/base32"
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"

	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
)

func defaultOpts(opts totp.ValidateOpts) totp.ValidateOpts {
	if opts.Period == 0 {
		opts.Period = 30
	}
	if opts.Skew < 0 {
		opts.Skew = 0
	} else if opts.Skew > 1 {
		opts.Skew = 1
	}
	if opts.Digits < 6 {
		opts.Digits = 6
	}
	if opts.Algorithm < 0 || opts.Algorithm > 3 {
		opts.Algorithm = otp.AlgorithmSHA1
	}
	return opts
}

func GenerateWithOpts(seed string, time time.Time, opts totp.ValidateOpts) (string, error) {
	opts = defaultOpts(opts)
	return totp.GenerateCodeCustom(seed, time, opts)
}

func Generate(seed string, time time.Time) (string, error) {
	return GenerateWithOpts(seed, time, totp.ValidateOpts{})
}

func NewSeed() string {
	var data [512]byte
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	r.Read(data[:])
	bytes := sha512.Sum512(data[:])
	sum512 := hex.EncodeToString(bytes[:])
	fmt.Println(sum512)
	sum512 = base32.StdEncoding.EncodeToString([]byte(sum512))
	return sum512
}
