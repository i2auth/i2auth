package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/pquerna/otp/totp"
	"gitlab.com/i2auth/i2auth/app/config"
	"gitlab.com/i2auth/i2auth/app/token"
	"golang.org/x/crypto/bcrypt"
)

func install() (err error) {
	seed := token.NewSeed()
	reader := bufio.NewReader(os.Stdin)

	text := `
#############################################
########   ########  ##################  ####
######   #   #####    #################  ####
#####   ###   ###  ##  ###########  ###  ####
#  #######   ###  ####  ##  #  ##    ##  ####
#########   ####  ####  ##  #  ###  ###    ##
#  #####   #####        ##  #  ###  ###  #  #
#  ####   ######  ####  ##  #  ###  ###  #  #
#  ###   #######  ####  ##  #  ###  ###  #  #
#  ##        ###  ####  ###    ###   ##  #  #
#############################################

Installation:

Please open our Telegram bot (http://t.me/i2authbot) and register yourself with the following command:

/register %s

It will generate an example code and a Hash for you that you need to introduce back here.
`
	fmt.Printf(text, seed)

	fmt.Printf("Please insert the example code: ")
	code, _ := reader.ReadString('\n')
	code = strings.TrimRight(code, "\n")

	generatedCode, err := token.GenerateWithOpts(seed, time.Now(), totp.ValidateOpts{
		Period: 120,
	})
	if err != nil {
		return errors.Wrap(err, "error generating the new code")
	}

	if code != generatedCode {
		return errors.New("the code you introduced is not valid, there's a mismatch with the internal generated one, please try again")
	}

	fmt.Printf("Please insert the Hash: ")
	hash, _ := reader.ReadString('\n')
	hash = strings.TrimRight(hash, "\n")

	err = bcrypt.CompareHashAndPassword([]byte(hash), []byte(seed))
	if err != nil {
		return errors.Wrap(err, "the hash is not correct, please try again")
	}

	err = config.Create(config.Config{
		Token:   seed,
		Hash:    hash,
		AuthUrl: "default",
	})
	if err != nil {
		return errors.Wrap(err, "error creating the config file")
	}

	fmt.Println("[i2auth] Successful installed.")
	return
}
