package config

import (
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Token   string
	Hash    string
	AuthUrl string
}

func Create(config Config) (err error) {
	configFile, err := FilePath()
	if err != nil {
		return errors.Wrap(err, "error retrieving the config file path")
	}

	file, err := os.OpenFile(configFile, os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return errors.Wrap(err, "the config file could not be opened")
	}
	defer file.Close()

	err = yaml.NewEncoder(file).Encode(config)
	if err != nil {
		return errors.Wrap(err, "error encoding the config file")
	}

	return
}

func Read() (config Config, err error) {
	configFile, err := FilePath()
	if err != nil {
		err = errors.Wrap(err, "could not determine config file")
		return
	}

	bytes, err := ioutil.ReadFile(configFile)
	if err != nil {
		err = errors.Wrap(err, "error reading the config file")
		return
	}

	err = yaml.Unmarshal(bytes, &config)
	if err != nil {
		err = errors.Wrap(err, "error encoding the config")
	}

	return
}

func FilePath() (configFile string, err error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		err = errors.Wrap(err, "the user home could not be found, thus the config can't be found")
		return
	}
	configFile = homeDir + string(os.PathSeparator) + ".i2auth"
	return
}

func Exists() bool {
	path, err := FilePath()
	if err != nil {
		return false
	}

	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}
