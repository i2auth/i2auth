# i2Auth PAM

Linux PAM module that executes a 2-Factor authentication verifying a code generated with the [TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm) algorithm.

## Contents table

- [i2Auth Server](#i2auth-pam)
  - [Contents table](#contents-table)
  - [Getting started](#getting-started)
  - [Requirements](#requirements)
  - [Build](#build)
  - [Instalation](#instalation)
  - [License](#license)

## Getting started

If you want to compile the module, follow the the [**Requirements**](#requirements) and the [**Installation**}]


**[Go up](#contents-table)**

### Requirements

The project uses [Golang][1] v1.12 at least, to compile the source code.
It also needs the PAM library required to create the module.

To install Golang go to the address, download the installer for your Operating System and execute it.
If you are using GNU/Linux, your distribution may provide you a package with the compiler.

#### ArchLinux

```sh
pacman -S go pam
```

#### Ubuntu
_There's no package for Debian Stable yet._
```sh
apt install golang libpam-dev
```

#### Fedora
```sh
dnf install golang libpam-devel
```

The file go.mod defines all the dependencies of the project. 
You don't need to install them as they will be downloaded before building the application.

The dependencies are the following:

- [Pkg Errors][2] for better error handling
- [otp][3] allows to create time-based One-Time-Password
- [crypto][4] allows us to create the tokens that will serve as validation
- [yaml][5] allows to parse yaml files


**[Go up](#contents-table)**

### Build

You need to compile the source code with Go.

You can do so by executing:
 
```
make build
```

### Instalation

If you want to install the PAM module in your machine, you need to copy the `i2auth.so` file you built in the previous step to the following folder which depends in your system.

#### ArchLinux
```
/usr/lib/security/i2auth.so
```

#### Ubuntu
```
/lib/x86_64-linux-gnu/security/i2auth.so
```

#### Fedora
```
/lib/security/i2auth.so
```

You would also need to modify the file `/etc/pam.d/sudo` and add the following line before anyother one, but under the `#%PAM-1.0` text:
This will enable the module to be executed when `sudo` runs. 

```
auth required i2auth.so
```


## License

This pam module uses the license GPLv3 - See [LICENSE](LICENSE) for more details.

**[Go Up](#contents-table)**

[1]: https://golang.org/
[2]: https://github.com/pkg/errors
[3]: https://github.com/pquerna/otp
[4]: https://golang.org/x/crypto
[5]: https://gopkg.in/yaml.v2
