package client

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/i2auth/i2auth/app/config"
)

const DEFAULT_AUTH_URL = "http://localhost:8080"

type Client interface {
	Check(ctx context.Context) (code string, err error)
}

type client struct {
	config config.Config
	client http.Client
}

type response struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func New(conf config.Config) *client {
	client := &client{
		config: conf,
		client: http.Client{
			Timeout: 1 * time.Minute,
		},
	}

	if client.config.AuthUrl == "default" {
		client.config.AuthUrl = DEFAULT_AUTH_URL
	}

	return client
}

func (c *client) Check(ctx context.Context) (code string, err error) {
	resp, err := c.request(ctx, "check")
	if err != nil {
		err = errors.Wrap(err, "error checking the code from the client")
		return
	}
	defer resp.Body.Close()

	var response response

	// If timeout occurs
	if resp.StatusCode == http.StatusRequestTimeout {
		err = errors.New("timeout while retrieving the code")
		return
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = errors.Wrap(err, "error reading from the response body")
		return
	}

	err = json.Unmarshal(bytes, &response)
	if err != nil {
		err = errors.Wrapf(err, "error unmarshaling response JSON from method check: %s", string(bytes))
		return
	}
	code = response.Code
	return
}

func (c *client) request(ctx context.Context, method string) (response *http.Response, err error) {
	values := url.Values{
		"hash": {c.config.Hash},
	}

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s", c.config.AuthUrl, method), strings.NewReader(values.Encode()))
	if err != nil {
		err = errors.Wrap(err, "error creating the request")
		return
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	response, err = c.client.Do(req)
	if err != nil {
		err = errors.Wrap(err, "error in the response")
		return
	}
	return
}
