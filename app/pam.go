package main

/*
#cgo LDFLAGS: -lpam -fPIC
#include <security/pam_appl.h>
#include <stdlib.h>

int pam_prompt(pam_handle_t *pamh, int style, char **response, const char *fmt);
int pam_get_user(pam_handle_t *pamh, const char **user, const char *prompt);
*/
import "C"
import (
	"context"
	"log"
	"sync"
	"time"

	"gitlab.com/i2auth/i2auth/app/client"
	"gitlab.com/i2auth/i2auth/app/config"
	"gitlab.com/i2auth/i2auth/app/token"
)

func main() {
}

//export pam_sm_authenticate
func pam_sm_authenticate(pamh *C.pam_handle_t, flags, argc C.int, argv **C.char) C.int {

	if !config.Exists() {
		err := install()

		if err != nil {
			log.Printf("%+v", err)
			return C.PAM_PERM_DENIED
		}

		return C.PAM_SUCCESS
	}

	conf, err := config.Read()
	if err != nil {
		log.Printf("%+v", err)
		return C.PAM_PERM_DENIED
	}

	newClient := client.New(conf)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	wg := sync.WaitGroup{}
	code, err := retrieveCode(ctx, &wg, newClient)
	cancel()
	if err != nil {
		log.Printf("%+v", err)
		return C.PAM_PERM_DENIED
	}

	validationCode, err := token.Generate(conf.Token, time.Now())
	if err != nil {
		log.Printf("%+v", err)
		return C.PAM_PERM_DENIED
	}
	wg.Wait()

	if code != validationCode {
		// Do not print code was not correct
		return C.PAM_PERM_DENIED
	}

	return C.PAM_SUCCESS
}
