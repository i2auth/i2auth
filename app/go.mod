module gitlab.com/i2auth/i2auth/app

go 1.12

require (
	github.com/boombuler/barcode v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/pquerna/otp v1.1.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	gopkg.in/yaml.v2 v2.2.2
)
