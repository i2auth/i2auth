package main

import "C"
import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/i2auth/i2auth/app/client"
)

func retrieveCode(ctx context.Context, wg *sync.WaitGroup, client client.Client) (code string, err error) {
	urlOut := make(chan string, 1)
	cliOut := make(chan string, 1)

	wg.Add(2)
	go retrieveFromURL(ctx, wg, client, urlOut)
	go retrieveFromCLI(ctx, wg, cliOut)

	for {
		select {
		case c, ok := <-urlOut:
			if ok {
				code = c
				fmt.Printf("%s. Press enter to continue...", c)
				return
			}
			urlOut = nil

		case c, ok := <-cliOut:
			if ok {
				code = c
				return
			}
			cliOut = nil

		case <-ctx.Done():
			err = errors.New("timeout while retrieving the code")
			return
		}
	}
}

func retrieveFromCLI(ctx context.Context, wg *sync.WaitGroup, out chan<- string) {
	defer close(out)
	defer wg.Done()

	fmt.Printf("[i2auth] token: ")

	reader := bufio.NewReader(os.Stdin)
	line, _ := reader.ReadString('\n')
	code := strings.TrimRight(line, "\n")
	out <- code
}

func retrieveFromURL(ctx context.Context, wg *sync.WaitGroup, client client.Client, out chan<- string) {
	defer close(out)
	defer wg.Done()

	code, err := client.Check(ctx)
	if err != nil {
		// No error should be printed, it can work offline
		return
	}

	if strings.TrimSpace(code) != "-1" {
		out <- code
		return
	}
}
